<?php

include_once "../connect.php"; 

?>

<!DOCTYPE html>
<html ng-app="myApp" ng-app lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IDrip | Customers</title>
<?php include("favicon.php"); ?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../plugins/css/font-awesome.min.css">
<link rel="stylesheet" href="../plugins/css/ionicons.min.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript" src="js/DT_bootstrap.js"></script>

</head>

<?php include("header.php"); ?>

<?php include("sidebar.php"); ?>

<div class="content-wrapper">

<section class="content-header">
<h1>
&nbsp;&nbsp;Customers Master List
<small>All Patients</small>
</h1>
<ol class="breadcrumb">
<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="index.php">Files</a></li>
<li class="active">Customers</li>
</ol>
</section>


<section class="content">


<div class="box-body">

<div class="box box-primary">
<div class="box-header with-border">
<h3 class="box-title">Type customer's name to search <a style="padding: 0px 0px 0px 650px" href="customer_add.php"><button class="btn btn-success addmore">Create New</button></a> </h3>
</div>


<div class="box">
<div class="box-body">
  <div class="table-responsive">
    <table class="table table-bordered table-hover" id="example">

      <thead>
        <tr>
          <th width="20%" style="text-align:center;">Name </th>
          <th width="25%" style="text-align:center;">Address</th>
          <th width="15%" style="text-align:center;">Phone No.</th>
          <th width="15%" style="text-align:center;">Email</th>
          <th width="20%" style="text-align:center;">Notes</th>
          <th style="text-align:center;"> Edit</th>
          <th style="text-align:center;"> Delete</th>
        </tr>
      </thead>
      <tbody>
        <?php


        $result = $dbo->prepare("SELECT * from customer  order by id DESC");

        $result->execute();
        for($i=0; $row = $result->fetch(); $i++);


          $query=mysqli_query($con, "SELECT id, name, address, phone, mail, notes  FROM `customer` ORDER BY id DESC")or die(mysqli_error($con));
        while($row=mysqli_fetch_array($query))  {

          ?>
          <tr>
            <td style="text-align:center;"><?php echo $row['name']; ?></td>
            <td style="text-align:center;"><?php echo $row['address']; ?></td>
            <td style="text-align:center;"><?php echo $row['phone']; ?></td>
            <td style="text-align:center;"><?php echo $row['mail']; ?></td>
            <td style="text-align:center;"><?php echo $row['notes']; ?></td>
            <td style="text-align:center;"><a  href="edit_customer.php<?php echo '?id='.$row['id']; ?>"><input type='submit'  class="btn btn-success addmore" value='Edit'> </a></td>

            <td style="text-align:center;"><a href="delete_customer.php?id=<?php echo $row['id']; ?>"><input type='submit'  type='submit' onClick="return confirm('Are you sure you want to Delete?');" class="btn btn-danger delete" value='Delete'> </a></td>


          </tr>
        <?php } ?>


      </div>
    </div>
  </tr>
</tbody>
</table>
</div>
</div>
</div>
</div>

<script src="js/angular.min.js"></script>
<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="app/app.js"></script>

  <script src="js/jquery.min.js"></script>
<script type="text/javascript">
function confirmDelete() 
{
var msg = "Are you sure you want to delete?";       
return confirm(msg);
}
</script>
<?php include_once("footer.php"); ?>
</body>
</html>