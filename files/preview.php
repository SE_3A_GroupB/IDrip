<?php

include_once "../connect.php"; 

?>

<!DOCTYPE html>
<html ng-app="myApp" ng-app lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IDrip | Receipt</title>

<?php include("favicon.php"); ?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../plugins/css/font-awesome.min.css">
<link rel="stylesheet" href="../plugins/css/ionicons.min.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

<style type="text/css">
#printable { display: none; }

@media print
{
#non-printable { display: none; }
#printable { display: block; }
}
</style>

<script language="javascript">
function printDiv(divName) 
{ 
var printContents = document.getElementById(divName).innerHTML; 
var originalContents = document.body.innerHTML; 
document.body.innerHTML = printContents; window.print(); 
document.body.innerHTML = originalContents; 
}
</script>


</head>
<?php
$invoice=$_GET['invoice'];
$result = $dbo->prepare("SELECT * FROM sales_list WHERE invoice= :userid ORDER BY transaction_id DESC");
$result->bindParam(':userid', $invoice);
$result->execute();
for($i=0; $row = $result->fetch(); $i++){

$invoice=$row['invoice'];
$date=$row['date'];
$am=$row['amount'];
$customer=$row['customer'];
}

?>




<?php include_once("header.php"); ?>

<?php include_once("sidebar.php"); ?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Official Receipt

</h1>
<ol class="breadcrumb">
<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="index.php">Files</a></li>
<li class="active">Receipt</li>
</ol>
</section>
<section class="content">

<div class="box-body">


<div class="box box-primary">

<div class="box">
<div class="box-header">

<a href="sales.php?id=cash&invoice=<?php echo $finalcode ?>"><button class="btn btn-success addmore"> Back to sales</button></a>
</div>
</br>
</br>

<div id="printableArea">


<div class="container">

<form action="actionpdf.php" method="post">
<div class="row">
<div class="col-xs-5">

	<img src="../uploads/<?php echo $logo; ?>" class="img-rounded" width="200px" /> </br>

</div>


<br><br><br>		

<div style= "font-size: 50px; margin: 0px 100px 0px 0px; padding: 0px 0px 0px 670px;color: white; background: #3572f6;"><span>OFFICIAL RECEIPT</span></div>

</div>


<br><br>
<div class="col-xs-5">
<p>
	Clinic: <?php echo $name ?><br>
	Address: <?php echo $address ?><br>
	Phone: <?php echo $phone ?> <br>
	E-mail: <font color = "blue"> <?php echo $email ?>   </font>
</p>		
</div>



<div class='col-xs-8 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-sm-4 col-md-4 col-lg-4'>
Date:  <?php echo date("d/m/Y"); ?> <br>			
Invoice No. <font color = "red"> <?php echo $invoice ?></font> <br>
Customer:  <?php echo $customer?> <br>
Prepared by: <?Php if(isset($_SESSION['userid'])){} echo "$_SESSION[userid]"; ?><br>


</div>


<div class='row'>
<div class="col-xs-10">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>

				<th width="35%">Trade Name</th>
				<th width="35%">Generic Name</th>
				<th width="10%">Quantity</th>
				<th width="10%">Unit Price</th>
				<th width="10%">Amount</th>

			</tr>
		</thead>
		<tbody>
			<?php
			$id=$_GET['invoice'];
			$result = $dbo->prepare("SELECT  i.trade_name, i.generic_name,  s.amount, s.price, s.quantity FROM `medicine_list` AS i INNER JOIN `sales_list` AS s ON i.sno = s.sno  WHERE invoice= :userid ");
			$result->bindParam(':userid', $id);
			$result->execute();
			for($i=0; $row = $result->fetch(); $i++){
				?>
				<tr class="record">
					<td><?php echo $row['trade_name']; ?></td>
					<td><?php echo $row['generic_name']; ?></td>
					<td><?php echo $row['quantity']; ?></td>
					<td>
						<?php
						$ppp=$row['price'];
						echo number_format($ppp, true);
						?>
					</td>

					<td>
						<?php
						$dfdf=$row['amount'];
						echo number_format($dfdf, true);
						?>
					</td>
				</tr>
				<?php
			}
			?>

			<tr>
				<td colspan="4" style=" text-align:right;"><strong>Total: &nbsp;<?php echo $currency  ?></strong></td>
				<td colspan="4">
					<?php
					$sdsd=$_GET['invoice'];
					$resultas = $dbo->prepare("SELECT sum(amount) FROM sales_list WHERE invoice= :a");
					$resultas->bindParam(':a', $sdsd);
					$resultas->execute();
					for($i=0; $rowas = $resultas->fetch(); $i++){
						$fgfg=$rowas['sum(amount)'];
						echo number_format($fgfg, true);
					}
					?>
				</td>
			</tr>
		</tbody>
	</table>
</div>
</div>


</div>



</div>
</div>

</form>
<input type="button" class="btn btn-primary "  onclick="printDiv('printableArea')" value="Print" />
<script src="js/angular.min.js"></script>
<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="app/app.js"></script>   
<script src="js/jquery.min.js"></script>  


<?php include_once("footer.php"); ?>    
</body>
</html>