<?php include_once('../connect.php');

?>
<!DOCTYPE html>
<html  lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IDrip | Expiry</title>

<?php include ("links.php"); ?>  

<style type="text/css">
#printable { display: none; }

@media print
{
#non-printable { display: none; }
#printable { display: block; }
}
</style>

<script language="javascript">
function printDiv(divName) 
{ 
var printContents = document.getElementById(divName).innerHTML; 
var originalContents = document.body.innerHTML; 
document.body.innerHTML = printContents; window.print(); 
document.body.innerHTML = originalContents; 
}
</script>

</head>


<?php include_once("header.php"); ?>
<?php include_once("sidebar.php"); ?>

<div class="content-wrapper">

<section class="content-header">
<h1>
Expired Medicine

</h1>
<ol class="breadcrumb">
<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="index.php">Files</a></li>
<li class="active">Inventory</li>
</ol>
</section>

<section class="content">
<div class="row">
<div class="col-xs-12">
<div id="printableArea">        
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">List of Expired Medicines</h3>
  </div>

  <div class="box-body">

    <div ng-controller="customersCrtl">

      <div class="row">
        <div class="col-md-12" ng-show="filteredItems > 0">
          <table id="example1" class="table table-striped table-bordered">
            <thead>

              <tr>
                <th style="text-align:center;">Brand Name</th>
                <th style="text-align:center;">Generic Name</th>
                <th style="text-align:center;">Expiry Date (yyyy-mm-dd)</th>
                <th style="text-align:center;">Quantity Left</th>
                <th style="text-align:center;">Cost</th>
                <th style="text-align:center;">Loss</th>

              </tr>
            </thead>
            <tbody>


              <?php 
               $query=mysqli_query($con, "CALL `expired`();")or die(mysqli_error());
            while($row=mysqli_fetch_array($query)){
                ?>  
                <tr>
                  <td style="width:240px;text-align:center;"><?php echo $row['trade_name']; ?></td>
                  <td style="width:240px;text-align:center;"><?php echo $row['generic_name']; ?></td>
                  <td style="width:250px;text-align:center;"><?php echo $row['expiry_date']; ?></td>
                  <td style="width:150px;text-align:center;"><?php echo $row['quantity']; ?></td>
                  <td style="width:100px;text-align:center;"><?php echo $row['cost_price']; ?></td>
                  <td style="width:220px;text-align:center;"><?php echo $row['loss']; ?></td>

                <?php } ?>
              </tr>
            </tbody>


            <thead>
              <tr>
                <th colspan="5" style="border-top:1px solid #999999;text-align:right"> Total Loss:</th>
                <th colspan="1" style="border-top:1px solid #999999;text-align:center"> 
                  <?php
                  function formatMoney($number, $fractional=false) {
                    if ($fractional) {
                      $number = sprintf('%.2f', $number);
                    }
                    while (true) {
                      $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
                      if ($replaced != $number) {
                        $number = $replaced;
                      } else {
                        break;
                      }
                    }
                    return $number;
                  }

                    $results = $dbo->prepare("SELECT c.trade_name, c.generic_name, i.quantity, i.expiry_date , i.batch , i.cost_price, sum(i.quantity * i.cost_price) AS tloss   FROM `medicine_list` AS c INNER JOIN `inventory` AS i ON c.sno = i.sno WHERE date_format(`expiry_date`, '%Y-%m-%d') < curdate()   AND  `quantity` > 0 ORDER BY expiry_date ASC"  );
                      $results->execute();
                      for($i=0; $rows = $results->fetch(); $i++){
                      $dsdsd=$rows['tloss'];
                      echo formatMoney($dsdsd, true);
                       }
                  ?>
                </th>
              </tr>
            </thead>
          </table>
        </div>


      </div>
    </div>
  </div>
</div>
</div>
<input type="button" class="btn btn-primary" onclick="printDiv('printableArea')" value="Print" />
<script src="js/angular.min.js"></script>
<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="app/app2.js"></script>     
<script src="js/jquery.min.js"></script>

<?php include_once("footer.php"); ?>    
</body>
</html>