<?php?>
<link rel="apple-touch-icon" sizes="180x180" href="../uploads/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="16x16" href="../uploads/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="32x32" href="../uploads/favicon-32x32.png">
<meta name="msapplication-config" content="../uploads/browserconfig.xml">
<link rel="shortcut icon" href="../uploads/favicon.ico">
<meta name="msapplication-TileColor" content="#00aba9">
<meta name="msapplication-config" content="../uploads/browserconfig.xml">
<link rel="mask-icon" href="../uploads/safari-pinned-tab.svg" color="#5bbad5">
<link rel="manifest" href="../uploads/site.webmanifest">