<?php

include_once "../connect.php"; 

$id = 1;
$stmt_edit = $dbo->prepare('SELECT * FROM settings WHERE id= :userid');
$stmt_edit->execute(array(':userid'=>$id));
$edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
extract($edit_row);


if(isset($_POST['register']))
{
$id = $_POST['memi'];
$a = $_POST['name'];
$b = $_POST['address'];
$c = $_POST['phone'];
$d = $_POST['email'];
$e = $_POST['country'];
$f = $_POST['address2'];
$g = $_POST['currency'];
$h = $_POST['level'];
$i = $_POST['timezone'];

$imgFile = $_FILES['user_image']['name'];
$tmp_dir = $_FILES['user_image']['tmp_name'];
$imgSize = $_FILES['user_image']['size'];

if($imgFile)
{
$upload_dir = '../uploads/'; // upload directory	
$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
$userpic = rand(1000,1000000).".".$imgExt;
if(in_array($imgExt, $valid_extensions))
{			
if($imgSize < 5000000)
{
unlink($upload_dir.$edit_row['image']);
move_uploaded_file($tmp_dir,$upload_dir.$userpic);
}
else
{
$errMSG = "Sorry, your file is too large it should be less then 5MB";
}
}
else
{
$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";		
}	
}
else
{

$userpic = $edit_row['logo']; 
}	

if(!isset($errMSG))
{
$sql = "UPDATE settings
SET name=?, address=?, phone=?, email=?, country=?, address2=?, currency=?, level=?, logo=?, timezone=?
WHERE id=?";
$q = $dbo->prepare($sql);
$q->execute(array($a,$b,$c,$d,$e,$f,$g,$h,$userpic,$i,$id));
header("location: settings.php");


}


}

?>

<!DOCTYPE html>
<html  lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>BestCare | Settings</title>

<?php include("favicon.php"); ?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">  
<link rel="stylesheet" href="../bootstrap/css/select2.css">  
<link rel="stylesheet" href="../bootstrap/css/select2-bootstrap.css">
<link rel="stylesheet" href="../plugins/css/font-awesome.min.css">
<link rel="stylesheet" href="../plugins/css/ionicons.min.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

</head>

<?php include("header.php"); ?>

<?php include("sidebar.php"); ?>

<div class="content-wrapper">

<section class="content-header">
<h1>&nbsp;&nbsp;Change Password
</h1>

<ol class="breadcrumb">
	<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="index.php">Files</a></li>
	<li class="active">Password</li>
</ol>
</section>

</section>

<section class="content">
<div class="box-body">

<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Change Password</h3>
	</div>

	<form role="form" method="post"  name='form1'  action="updatepassword.php"  onsubmit="return validate()">
		<div class="box-body">
			<div class="form-group">
				<label for="exampleInputEmail1">&nbsp;Old Password</label>
				<input type="hidden" name="todo" value="change-password">
				<input type="password"  name="old_password" class="form-control" id="skills" placeholder="Enter Old password"  required>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">&nbsp;New Password</label>
				<input type="password"  name="password"  class="form-control" id="checkleng" placeholder="Password must be 6 or more characters" required>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">&nbsp;Re-Enter new Password</label>
				<input type="password"  name="password2"  class="form-control" id="checkleng" placeholder="Repeat password" required>
			</div>


			<div>
				<button type="submit" name="submit" class="btn btn-primary">Change Password</button>

			</div>
		</div>


	</form>
</br>
</br>
</br>
<br><br><br><br><br><br>
</div>
</div>
</div>

<script src="js/angular.min.js"></script>
<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="app/app.js"></script>   
<script src="js/jquery.min.js"></script> 
<script src="//select2.github.io/select2/select2-3.4.2/select2.js"></script>
<script>
$( ".select2" ).select2( { placeholder: "Select a State", maximumSelectionSize: 6 } );

$( ":checkbox" ).on( "click", function() {
$( this ).parent().nextAll( "select" ).select2( "enable", this.checked );
});

$( "#demonstrations" ).select2( { placeholder: "Select2 version", minimumResultsForSearch: -1 } ).on( "change", function() {
document.location = $( this ).find( ":selected" ).val();
} );

$( "button[data-select2-open]" ).click( function() {
$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
});
</script>

<script type="text/javascript">
function confirmDelete() 
{
var msg = "Are you sure you want to delete?";       
return confirm(msg);
}
</script>  

<script type='text/javascript'>
function validateForm() {
var x = document.forms["form1"]["old_password"].value || document.forms["form1"]["password"].value|| document.forms["form1"]["password2"].value;

if (x == null || x == "") {
	alert("Field must be filled out");
	return false;
}
}
</script>

<script type='text/javascript'>
function validate(){
var checkleng = document.getElementById("checkleng");
if(checkleng.value.length <= 15 && checkleng.value.length >= 6){
}
else{
	alert("Make sure the new password is between 6-15 characters long");
	this.focus();
	return false;
}
}
</script>
<?php include("footer.php"); ?>    
</body>
</html>
