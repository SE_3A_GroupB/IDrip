<?php

include_once "../connect.php";

?>
<!DOCTYPE html>
<html  lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IDrip | Appointment</title>

<?php include_once("links.php"); ?>

</head>

<?php include_once("header.php"); ?>
<?php include_once("sidebar.php"); ?>

<div class="content-wrapper">

<section class="content-header">
<h1>
Edit Appointment
</h1>
<ol class="breadcrumb">
<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="index.php">Files</a></li>
<li class="active">Appointments</li>
</ol>
</section>

<section class="content">

<div class="box-body">

<div class="box box-primary">
<div class="box-header with-border">
</div>

<?php

$id=$_GET['id'];
$result = $dbo->prepare("SELECT * FROM appointment WHERE id= :userid");
$result->bindParam(':userid', $id);
$result->execute();
for($i=0; $row = $result->fetch(); $i++){
?>

<form role="form" method="post" action="updateappointment.php" >
  <div class="box-body">
    <div class="form-group">
      <label for="exampleInputEmail1">Customer</label>
      <input type="hidden" name="memi" value="<?php echo $id; ?>" />

      <input type="text"  name="customer" class="form-control" id="skills"  value="<?php echo $row['customer']; ?>"  readonly>
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Date</label>
      <input type="text"  id="to" name="date" class="form-control" id="skills"  value="<?php echo $row['date']; ?>">
    </div>


    <div class="form-group">
      <label for="exampleInputEmail1">Time</label>
      <input type="text" id="to" name="time" class="form-control" id="checkleng"  value="<?php echo $row['time']; ?>">
    </div>
  </div>
  <div>&nbsp;&nbsp;&nbsp;
    <button type="submit" name="register" class="btn btn-primary">Save</button>
  </div>

</br>

</br>

</form>		   
<?php
}
?> 
</div>
</div>
<script src="js/angular.min.js"></script>
<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="app/app.js"></script>   
<script src="js/jquery.min.js"></script>  

<?php include_once("footer.php"); ?>    
</body>
</html>