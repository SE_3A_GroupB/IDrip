<?php

include "../connect.php";

?>

<!DOCTYPE html>
<html  lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IDrip | Users</title>

<?php include("links.php"); ?> 

<script type='text/javascript'>
  function validateForm() {
    var x = document.forms["form1"]["name"].value || document.forms["form1"]["userid"].value|| document.forms["form1"]["password"].value
    || document.forms["form1"]["level"].value; || document.forms["form1"]["mail"].value;

    if (x == null || x == "") {
      alert("Field must be filled out");
      return false;
    }
  }
</script>

<script>
  function myFunction() {
    document.getElementById("students").reset();
  }
</script>

</head>

<?php include("header.php"); ?>

<?php include("sidebar.php"); ?>

<div class="content-wrapper">

  <section class="content-header">
    <h1>
      &nbsp;&nbsp;Add Users
      <small>Create new system users</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="index.php">Files</a></li>
      <li class="active">Add Users</li>
    </ol>
  </section>

  <section class="content">

    <div class="box-body">

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">All Fields are required</h3>
        </div>

        <form role="form" method="post"  name='form1'  action="signuplist.php">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Full Name</label>
              <input type="text"  name="name" class="form-control" id="skills" placeholder="Enter Full Name"  required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input type="text"  name="userid"  class="form-control" id="checkleng" placeholder="Username must be 6 or more characters" required>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">E-mail</label>
              <input type="text"  name="mail"  class="form-control" id="checkleng" placeholder="Enter your E-mail" required>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Password</label>
              <input type="text"  name="password"  class="form-control" id="checkleng" placeholder="Enter your password" required>
            </div>

            <div>
              <button type="submit" name="register" class="btn btn-primary">Register</button>

            </div>
          </div>
          <br><br><br><br><br><br>
        </form>

        <?php
        if (isset($_POST['register'])){
          $name=$_POST['name'];
          $userid=$_POST['userid'];
          $password=$_POST['password'];
          $level=$_POST['level'];
          $mail=$_POST['mail'];

          mysqli_query($con, "insert into users (name,userid,password,level, mail) values('$name','$userid','$password','$level','$mail')")or die(mysqli_error());

        }
        ?>

        <?php include("footer.php"); ?>    
      </body>
      </html>