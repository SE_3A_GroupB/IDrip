<?php
include_once('../connect.php');

// ../system users
        $result = $dbo->prepare("SELECT * FROM `users`");
        $result->execute();
        $rowcount = $result->rowcount();

// ../number of products
        $result = $dbo->prepare("SELECT c.trade_name, c.generic_name,  i.id, sum(i.quantity) AS qty1 , sum(i.qty_sold) AS qty2 FROM `medicine_list` AS c INNER JOIN `inventory` AS i ON c.sno = i.sno GROUP BY i.sno ORDER BY i.id Desc");
        $result->execute();
        $rowcount12 = $result->rowcount();
// ../customers
        $result = $dbo->prepare("SELECT * FROM `customer`");
        $result->execute();
        $rowcount1234 = $result->rowcount();

// ../appointment
        $result = $dbo->prepare( "SELECT * FROM `appointment` WHERE DATE(date) = CURDATE();")or die(mysqli_error());
        $result->execute();
        $rowcount1 = $result->rowcount();

// ../out of stock
        $result = $dbo->prepare("SELECT c.trade_name,c.generic_name,i.expiry_date, sum(i.quantity) AS Qty FROM `medicine_list` AS c INNER JOIN `inventory` AS i ON c.sno = i.sno  GROUP BY i.sno HAVING sum(i.quantity) <= 10");
        $result->execute();
        $rowcount123 = $result->rowcount();

?>





<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
  <div class="user-panel">
        <div class="pull-center image" style= "margin: 0px 0px 0px 0px; padding: 0px 0px 0px 15px;">
          <img src="../uploads/<?php echo $logo; ?>" width="150px" height="150px" />
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header" style="padding: 5px 5px 5px 70px; color: white">MAIN NAVIGATION</li>
        <li>
          <a href="index.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span>
              <i></i>
            </span>
          </a>
        </li>

        <li class="treeview">
          <a href="appointment.php">
            <i class="fa fa-calendar"></i> 
            <span>Appointment</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="appointment.php"><i class="fa fa-circle-o"></i>List of Appointments</a></li>

            <li><a href="appointment_add.php"><i class="fa fa-circle-o"></i>Add Appointment</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="sales.php?id=cash&invoice=<?php echo $finalcode ?>">
            <i class="fa fa-money"></i>
            <span>Point of Sale</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
        <li><a href="sales.php?id=cash&invoice=<?php echo $finalcode ?>"><i class="fa fa-circle-o"></i> Create POS</a></li>
            <li><a href="sales_records.php"><i class="fa fa-circle-o"></i>View POS</a></li>

          </ul>
        </li>


	<li>
          <a href="customer.php">
            <i class="fa fa-smile-o"></i> <span>Customer</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="customer.php"><i class="fa fa-circle-o"></i>Customer's List</a></li>

            <li><a href="customer_add.php"><i class="fa fa-circle-o"></i>Add Customer</a></li>
          </ul>
        </li>


        <li>
          <a href="products.php">
            <i class="fa fa-cubes"></i> <span>Product Inventory</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="products.php"><i class="fa fa-circle-o"></i>Inventory</a></li>
            <li><a href="medicine_list.php"><i class="fa fa-circle-o"></i>Medicine List</a></li>
			<li><a href="medicine_edit.php"><i class="fa fa-circle-o"></i>Edit Medicine List</a></li>
			<li><a  href="add_medicine.php"><i class="fa fa-circle-o"></i>Add Stock</a></li>
            <li><a href="inventory.php"><i class="fa fa-circle-o"></i>Purchase History</a></li>
			<li><a href="out_of_stock.php"><i class="fa fa-circle-o"></i>Out of Stock</a></li>
            <li><a href="expiry.php"><i class="fa fa-circle-o"></i>Soon Expiring</a></li>
			 <li><a href="expired.php"><i class="fa fa-circle-o"></i>Expired Medicine</a></li>
          </ul>
        </li>



  <li>
          <a href="signuplist.php">
            <i class="fa fa-user"></i> <span>System Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="signuplist.php"><i class="fa fa-circle-o"></i>List of User</a></li>
            <li><a href="signup.php"><i class="fa fa-circle-o"></i>Add User</a></li>
          </ul>
        </li>

          <li>
          <a href="supplier.php">
            <i class="fa fa-area-chart"></i> <span>Supliers</span>
            <span >
              <i ></i>
            </span>
          </a>
         
        </li>

      <li>
          <a href="settings.php">
            <i class="fa fa-gear"></i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="settings.php"><i class="fa fa-circle-o"></i>Profile Setting</a></li>
            <li><a href="settings_changepass.php"><i class="fa fa-circle-o"></i>Change Password</a></li>
          </ul>
        </li>

           <li>
          <a href="../logout.php">
            <i class="fa fa-sign-out"></i> <span>Log Out</span>

          </a>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
