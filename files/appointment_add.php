<?php

include_once "../connect.php";

?>

<!DOCTYPE html>
<html  lang="en">
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IDrip | Appointment </title>
 
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/chosen.min.css" rel="stylesheet" media="screen">
<link href="css/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="apple-touch-icon" sizes="180x180" href="../uploads/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="16x16" href="../uploads/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="32x32" href="../uploads/favicon-32x32.png">
<link rel="manifest" href="../uploads/site.webmanifest">
<link rel="mask-icon" href="../uploads/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="../uploads/favicon.ico">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../bootstrap/css/select2.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="../plugins/css/font-awesome.min.css">
<link rel="stylesheet" href="../plugins/css/ionicons.min.css">
<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
<link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="../plugins/morris/morris.css">
<link rel="stylesheet" href="../plugins/timepicker/bootstrap-timepicker.css">
<link rel="stylesheet" href="css/jquery-ui.min.css">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="msapplication-TileColor" content="#00aba9">
<meta name="msapplication-config" content="../uploads/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8" language="javascript" src="js/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript" src="js/jquery.dataTables.js"></script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
   $(function(){
        $("#to").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#from").datepicker({ dateFormat: 'yy-mm-dd' }).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
            minValue.setDate(minValue.getDate());
            $("#to").datepicker( "option", "minDate", minValue );
        })
    });
  </script>


</head>


<?php include("header.php"); ?>

<?php include("sidebar.php"); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
          &nbsp;&nbsp;Add Appointment
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="index.php">Files</a></li>
        <li class="active">Appointment</li>
      </ol>
    </section>
    <section class="content">
      
        <div class="box-body">
          <div class="box box-primary">
             <div class="box-header with-border">
          <h3 class="box-title">Create new Appointment <a style="padding: 0px 0px 0px 780px";" href="appointment.php"><button class="btn btn-success addmore">View List</button></a></h3>
  </div>
      
  <div class="box">
    <div class="box-body">
           <form role="form" method="post" name='form1'  action="appointmentadd_validate.php" >
              <div class="box-body">

                 <div class="row">
                <div class="col-xs-12">
                  <label for="exampleInputEmail1">Customer</label><span style="padding: 0px 0px 0px 750px">Customer's not in the list? <a href="customer_add.php"><u><i>Click Here</i></u></a></span><br>
                   <select name="customer"  id="select2"  style="width:1030px">
                  <option></option>
                 <?php
                 $result3 = $dbo->prepare("SELECT * FROM customer");
                 $result3->bindParam(':userid', $res);
                 $result3->execute();
                 for($i=0; $row = $result3->fetch(); $i++){
                 ?>
                 <option><?php echo $row['name']; ?></option>
                 <?php
                 }
                 ?>
                </select>
                </div>
</div><br>
                <div class="row">
                  <div class="col-xs-6">
                  <label for="exampleInputEmail1">Date</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="text" style="width:500px" id= "from" name="date" value="<?php echo date("Y-m-d");?> " />&nbsp;&nbsp;&nbsp;
                </div>
                  <div class="col-xs-6">
                  <label for="exampleInputEmail1">Time</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="text" style="width:500px" name="time" value="<?php echo date("h:i a"); ?>" />&nbsp;
                </div>
                  </div>
                <div>&nbsp;
                <a style="padding: 0px 0px 0px 450px"><button type="submit" name="register" class="btn btn-primary">Save Appointment</button></a>
                </div>
      
         </br>
         </br>
         </form>
      
      <?php
        if (isset($_POST['register'])){
        $customer=$_POST['customer'];
        $date=$_POST['date'];
        $time=$_POST['time'];
        
        mysqli_query($con, "INSERT into appointment (customer,date,time) values('$customer','$date','$time')")or die(mysqli_error($con));
        
        }
        ?>
<br><br><br><br><br>      
<script src="js/angular.min.js"></script>
<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="app/app.js"></script> 
<script>
    $(function(){
    $('#select2').select2();
    });
</script>
<script src="js/chosen.jquery.min.js"></script>
<script>
        $(function() {
        $(".uniform_on").uniform();
        $(".chzn-select").chosen();
        $('.textarea').wysihtml5();

        });
</script>
<br><br><br><br><br>  
<?php include ("footer.php"); ?>    
</body>
</html>