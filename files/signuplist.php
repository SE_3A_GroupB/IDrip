<?php

include "../connect.php";

?>

<!DOCTYPE html>
<html  lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IDrip | Users</title>

<?php include("favicon.php"); ?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../plugins/css/font-awesome.min.css">
<link rel="stylesheet" href="../plugins/css/ionicons.min.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">



</head>

<?php include("header.php"); ?>

<?php include("sidebar.php"); ?>

<div class="content-wrapper">

  <section class="content-header">
    <h1>
      Users Master List
      <small>All users</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="index.php">Files</a></li>
      <li class="active">Users</li>
    </ol>
  </section>

  <section class="content">

    <div class="box-body">

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Users according to roles</h3>
        </div>

        <?php
        if (isset($_POST['register'])){
          $name=$_POST['name'];
          $userid=$_POST['userid'];
          $password1=$_POST['password'];
          $mail=$_POST['mail'];
          $password=md5($password1);
          $url = "http://" . $_SERVER['SERVER_NAME'] ;


          require_once("phpmailer/class.phpmailer.php");

          $mailer = new PHPMailer();
          $mailer->From = ' '.$email.' ';
          $mailer->FromName = 'IDrip';
          $mailer->Body = 'Hello '.$name.', Your username: '.$userid.'  | Your password is: '.$password1.'. Please login here '.$url.' and change your password';
          $mailer->Subject = 'New System Account created';
          $mailer->AddAddress($_POST['mail']); 


          if(!$mailer->Send())
          {
            echo "<script>alert('Registration failed.')</script>";

          }
          else
          {
            echo "<script>alert('Registration Success! Login details have been sent to user's E-mail.')</script>"; 

            mysqli_query($con, "insert into users (name,userid,password,mail) values('$name','$userid','$password','$mail')")or die(mysqli_error($con));

          }

        }
        ?>



        <section class="content">
          <div class="row">
            <div class="col-xs-12">

            </div>

            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th style="text-align: center;">Full Name</th>
                    <th style="text-align: center;">E-mail</th>
                    <th style="text-align: center;">Username</th>
                    <th style="text-align: center;">Edit</th>
                    <th style="text-align: center;">Delete</th>
                  </tr>
                </thead>

                <tbody>
                  <?php 

                  $query=mysqli_query($con, "SELECT name, id, mail, userid, password  FROM `users` ORDER BY id DESC  LIMIT 10")or die(mysqli_error($con));
                  while($row=mysqli_fetch_array($query)){
                    ?>
                    <tr>
                      <td style="text-align: center;"><?php echo $row['name']; ?></td>
                      <td style="text-align: center;"><?php echo $row['mail']; ?></td>
                      <td style="text-align: center;"><?php echo $row['userid']; ?></td>
                      <td style="text-align: center;"><a href="edit_user.php?id=<?php echo $row['id']; ?>"><input type='submit' class="btn btn-success addmore" value='Edit'> </a></td>
                      <td style="text-align: center;"><a href="delete_user.php?id=<?php echo $row['id']; ?>"><input type='submit'  type='submit' onClick="return confirm('Are you sure you want to Delete?');" class="btn btn-danger delete" value='Delete'>  </a></td>


                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          <br><br><br><br><br><br>
        </div>
        <br><br><br><br><br><br>
      </div>
    </div>
    <script src="js/angular.min.js"></script>
    <script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
    <script src="app/app.js"></script>   
    <script src="js/jquery.min.js"></script> 


    <script type="text/javascript">
      function confirmDelete() 
      {
        var msg = "Are you sure you want to delete?";       
        return confirm(msg);
      }
    </script>  

    <?php include("footer.php"); ?>    
  </body>
  </html>