<body class="hold-transition skin-blue sidebar-mini">

  <div class="wrapper">

    <header class="main-header">

      <a href="index.php" class="logo">

        <span class="logo-mini"><b>IDRIP</b></span>

        <span class="logo-lg"><b>IDrip Gluta Center</b></span>
      </a>

      <nav class="navbar navbar-static-top">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>

        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav"> 

            <li class="dropdown user user-menu">
              <span class="hidden-xs">

                <p><form action="#" method="POST" id="search-form" class="fr">
                  <fieldset><?php require "check.php"; ?> </fieldset>
                </form>
              </p>

            </span>

          </li>

        </ul>
      </div>
    </nav>
  </header>