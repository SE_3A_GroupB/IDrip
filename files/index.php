
<?php include_once ('../connect.php');

$userid = $_SESSION['userid'];
$login = mysqli_query($con, "select * from users where userid='$userid'")or die(mysqli_error());
$row=mysqli_fetch_row($login);
$level = $row[4];

if ($level == '')
{
header('location:../index.php');
}
?>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IDrip | Dashboard</title>

<?php include("links.php"); ?>  

</head>
<?php include("header.php"); ?>


<?php include("sidebar.php"); ?>


<div class="content-wrapper">

<section class="content-header">
<h1>
Dashboard
<small>Control panel</small>
</h1>
<ol class="breadcrumb">
<li><a href="../#"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Dashboard</li>
</ol>
</section>


<section class="content">
<div class="row">
<div class="col-lg-3 col-xs-6">
  <div class="small-box bg-aqua ">
    <div class="inner">
      <h3><?php echo $rowcount12;?></h3>
      <p>Number of Products</p>
    </div>
    <div class="icon">
      <i class="ion ion-bag" style="padding: 5px 0px 0px 0px"></i>
    </div>
    <a href="products.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div>


<div class="col-lg-3 col-xs-6">
  <div class="small-box bg-yellow">
    <div class="inner">
      <?php 
      $result = $dbo->prepare("CALL `out_of_stock`();");
        $result->execute();
        $rowcount123 = $result->rowcount();
         ?>
      <h3><?php echo $rowcount123;?></h3>
      <p>Out of Stock</p>

    </div>
    <div class="icon">
      <i class="ion ion-pie-graph" style="padding: 5px 0px 0px 0px"></i>
    </div>
    <a href="out_of_stock.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div>




<div class="col-lg-3 col-xs-6">

  <div class="small-box bg-orange">
    <div class="inner">
      <?php $result = $dbo->prepare("CALL `expiry`();")or die(mysql_error());
        $result->execute();
        $rowcount12345 = $result->rowcount();
            ?>
              <h3><?php echo $rowcount12345;?></h3>

              <p>Soon Expiring</p>
    </div>
    <div class="icon">
      <i class="ion ion-android-notifications" style="padding: 3px 0px 0px 0px"></i>
    </div>
    <a href="expiry.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div>




<div class="col-lg-3 col-xs-6">
  <div class="small-box bg-red">
    <div class="inner">
      <?php $result = $dbo->prepare("CALL `expired`();")or die(mysql_error());
        $result->execute();
        $rowcount123456 = $result->rowcount();
            ?>
      <h3><?php echo $rowcount123456;?></h3>
      <p>Expired Products</p>
    </div>
    <div class="icon">
      <i class="ion ion-alert-circled"></i>
    </div>
    <a href="expired.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div>




<div class="col-lg-3 col-xs-6">
  <div class="small-box bg-green">
    <div class="inner">
      <h3><?php echo $rowcount1234;?></h3>
      <p>Registered Customers</p>

    </div>
    <div class="icon">
      <i class="ion ion-person"></i>
    </div>
    <a href="customer.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div>

<?php


function formatMoney($number, $fractional=false) {
  if ($fractional) {
    $number = sprintf('%.2f', $number);
  }
  while (true) {
    $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
    if ($replaced != $number) {
      $number = $replaced;
    } else {
      break;
    }
  }
  return $number;
}


$result = $dbo->prepare("SELECT * FROM users ");
$result->execute();
$rowcount = $result->rowcount();


$query = mysqli_query($con, "SELECT sum(amount) FROM `sales_list` WHERE  DATE(datetime) = CURDATE();")or die(mysqli_error());
while($row = mysqli_fetch_array($query)){
  ?>


  <section class="content">

    <div class="row">
      <div class="col-lg-3 col-xs-6">

        <div class="small-box bg-purple">
          <div class="inner">
            <h3><?php echo $currency  ?> <?php echo formatMoney($row['sum(amount)']); ?></h3>
          <?php } ?>
          <p>Today's Total Sales</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="sales_records.php?d1=0&d2=0" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>


    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-blue">
        <div class="inner">
          <h3><?php echo $rowcount1;?></h3>
          <p>Today's Appointment</p>

        </div>
        <div class="icon">
          <i class="ion ion-calendar" style="padding: 5px 0px 0px 0px"></i>
        </div>
        <a href="appointment_today.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-teal">
        <div class="inner">
          <h3><?php echo $rowcount;?></h3>

          <p>System Users</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="signuplist.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>

  <div class="row">
    <section class="col-xs-10 connectedSortable">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
          <li class="active"><a href="../#revenue-chart" data-toggle="tab">Summary</a></li>

          <li class="pull-left header"><i class="fa fa-inbox"></i> Total Monthly Sales</li>
        </ul>
        <div class="tab-content no-padding">
          <table class="table table-bordered table-striped" id="table_example" data-responsive="table" >
            <thead>
              <tr>
                <th> Year  </th>
                <th> Month </th>
                <th> Total Sales</th>

              </tr>
            </thead>
            <tbody>




              <?php


              $start_date =  date('Y-01-01');

              $end_date =  date('Y-12-31');

              $query=mysqli_query($con, "SELECT YEAR(datetime) AS `Year`, MONTHNAME(datetime) AS `Month`, SUM(amount) AS `Total` FROM sales_list WHERE datetime BETWEEN '" . $start_date . "' AND '" . $end_date . "' GROUP BY YEAR(datetime), MONTH(datetime)")or die(mysqli_error());

              while($row=mysqli_fetch_array($query)){


                ?>

                <td><?php echo $row['Year']; ?></td>
                <td><?php echo $row['Month']; ?></td>
                <td><span class="white-text" style="margin-right: 0.1em;"><?php echo $currency  ?></span><?php echo number_format ($row['Total']); ?></td>


              </tr>
              <?php
            }
            ?>

          </tbody>

        </table>

      </div>
    </div>

  </div>
</div>
</section>

</div>


</div>
</div>
</div>
</div>
</div>
</section>
</div>

</section>
</div>

<script src="js/jquery.js" type="text/javascript"></script>
<script type="application/javascript" src="js/awesomechart.js"> </script>
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../plugins/jQuery/raphael-min.js"></script>
<script src="../plugins/morris/morris.min.js"></script>
<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="../plugins/knob/jquery.knob.js"></script>
<script src="../plugins/jQuery/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../plugins/fastclick/fastclick.js"></script>
<script src="../dist/js/app.min.js"></script>
<script src="../dist/js/pages/dashboard.js"></script>
<script src="../dist/js/demo.js"></script>
<?php include("footer.php"); ?>
</body>
</html>