<?php?>

<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/chosen.min.css" rel="stylesheet" media="screen">
<link href="css/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="apple-touch-icon" sizes="180x180" href="../uploads/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="16x16" href="../uploads/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="32x32" href="../uploads/favicon-32x32.png">
<link rel="manifest" href="../uploads/site.webmanifest">
<link rel="mask-icon" href="../uploads/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="../uploads/favicon.ico">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../bootstrap/css/select2.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="../plugins/css/font-awesome.min.css">
<link rel="stylesheet" href="../plugins/css/ionicons.min.css">
<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
<link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="../plugins/morris/morris.css">
<link rel="stylesheet" href="../plugins/timepicker/bootstrap-timepicker.css">
<link rel="stylesheet" href="css/jquery-ui.min.css">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="msapplication-TileColor" content="#00aba9">
<meta name="msapplication-config" content="../uploads/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8" language="javascript" src="js/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript" src="js/jquery.dataTables.js"></script>

<script>
$(function(){
$("#to").datepicker({ dateFormat: 'yy-mm-dd' });
$("#from").datepicker({ dateFormat: 'yy-mm-dd' }).bind("change",function(){
var minValue = $(this).val();
minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
minValue.setDate(minValue.getDate());
$("#to").datepicker( "option", "minDate", minValue );
})
});
</script>