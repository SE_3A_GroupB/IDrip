-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2019 at 06:53 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `idrip1`
--
CREATE DATABASE IF NOT EXISTS `idrip1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `idrip1`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `appointment_check`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `appointment_check` ()  SELECT *, 
ADDTIME(
  CONCAT(time,'.0'), 
  CONCAT(CONCAT(CONCAT( length DIV 60, ':' ), length MOD 60) ,':0.0') 
) AS EndTime 
FROM `appointment`$$

DROP PROCEDURE IF EXISTS `expired`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `expired` ()  SELECT c.trade_name, c.generic_name, i.quantity, i.expiry_date , i.batch , i.cost_price, (i.quantity * i.cost_price) AS loss   FROM `medicine_list` AS c INNER JOIN `inventory` AS i ON c.sno = i.sno WHERE date_format(`expiry_date`, '%Y-%m-%d') <= curdate()   AND  `quantity` > 0 ORDER BY expiry_date ASC$$

DROP PROCEDURE IF EXISTS `expiry`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `expiry` ()  SELECT c.trade_name, c.generic_name, i.quantity, i.expiry_date , i.batch  FROM `medicine_list` AS c INNER JOIN `inventory` AS i ON c.sno = i.sno WHERE date_format(`expiry_date`, '%Y-%m-%d') < NOW() + INTERVAL 3 MONTH  AND  `quantity` > 0 AND date_format(`expiry_date`, '%Y-%m-%d') > curdate() ORDER BY expiry_date ASC$$

DROP PROCEDURE IF EXISTS `out_of_stock`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `out_of_stock` ()  SELECT c.trade_name,c.generic_name,i.expiry_date, sum(i.quantity) AS Qty FROM `medicine_list` AS c INNER JOIN `inventory` AS i ON c.sno = i.sno  GROUP BY i.sno HAVING sum(i.quantity) <= 10$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
CREATE TABLE `appointment` (
  `id` int(7) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `date` varchar(20) NOT NULL,
  `time` varchar(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `appointment`:
--

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `customer`, `date`, `time`, `created`) VALUES
(3, 'Jumar Dulay', '2018-11-30', '1:30 pm', '0000-00-00 00:00:00'),
(6, 'Leah A. Juan', '2018-12-01', '09:00 am', '0000-00-00 00:00:00'),
(7, 'Jumar Dulay', '2018-11-30', '08:34 am', '0000-00-00 00:00:00'),
(9, 'Valerie A. Arizobal', '2018-11-30', '01:57 pm', '0000-00-00 00:00:00'),
(10, 'Donald Arienza Juan', '2018-11-30', '04:00 pm', '0000-00-00 00:00:00'),
(11, 'Valerie A. Arizobal', '2018-11-29', '05:12 pm', '2018-12-08 06:36:58'),
(12, 'John Jay Rivera', '2018-11-30', '11:02 am', '0000-00-00 00:00:00'),
(13, 'Valerie A. Arizobal', '2018-12-03', '11:00 am', '0000-00-00 00:00:00'),
(14, 'Leafe A. Consigna', '2018-11-29 ', '1:45 pm', '2018-12-15 05:11:59'),
(16, 'Ivy D. Paloma', '2018-12-01 ', '08:00 am', '0000-00-00 00:00:00'),
(17, 'Leafe A. Consigna', '2018-12-01 ', '10:30 am', '0000-00-00 00:00:00'),
(18, 'Leah A. Juan', '2018-12-15', '11:49 am', '0000-00-00 00:00:00'),
(19, 'Donald', '2018-12-06 ', '02:01 am', '0000-00-00 00:00:00'),
(20, 'Donald', '2018-12-07', '02:01 am', '0000-00-00 00:00:00'),
(21, 'Donald', '2018-12-30', '07:00 am', '0000-00-00 00:00:00'),
(22, 'John Jay', '2018-12-07', '05:37 am', '0000-00-00 00:00:00'),
(23, 'Joven A. Juan', '2018-12-07', '07:00 am', '0000-00-00 00:00:00'),
(24, 'Leah A. Juan', '2018-12-12', '10:17 pm', '0000-00-00 00:00:00'),
(43, 'Joven A. Juan', '2018-12-08 ', '08:52 am', '2018-12-08 05:52:49'),
(44, 'Leah A. Juan', '2018-12-30', '09:00 am', '2018-12-08 06:02:21'),
(46, 'Donald A. Juan', '2018-12-08 ', '09:10 am', '2018-12-08 06:10:28'),
(47, 'Leah A. Juan', '2018-12-08 ', '09:18 am', '2018-12-08 06:18:18'),
(49, 'Ivy D. Paloma', '2018-12-08 ', '07:41 pm', '2018-12-08 11:42:22'),
(50, 'Joven A. Juan', '2018-12-15 ', '10:44 am', '2018-12-15 01:45:01'),
(51, 'Mark G. Lumen', '2018-12-30', '12:55 pm', '2018-12-15 08:31:14'),
(52, 'Mark Lumen', '2018-12-16 ', '3:44 pm', '2018-12-16 04:44:10');

--
-- Triggers `appointment`
--
DROP TRIGGER IF EXISTS `appointment_AD`;
DELIMITER $$
CREATE TRIGGER `appointment_AD` AFTER DELETE ON `appointment` FOR EACH ROW insert into appointment_logs values (null, old.id,old.customer,'Deleted', NOW())
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `appointment_AI`;
DELIMITER $$
CREATE TRIGGER `appointment_AI` AFTER INSERT ON `appointment` FOR EACH ROW insert into appointment_logs values (null, new.id,new.customer,'Inserted', NOW())
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `appointment_AU`;
DELIMITER $$
CREATE TRIGGER `appointment_AU` AFTER UPDATE ON `appointment` FOR EACH ROW insert into appointment_logs values (null, new.id,old.customer,'Updated', NOW())
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `appointment_logs`
--

DROP TABLE IF EXISTS `appointment_logs`;
CREATE TABLE `appointment_logs` (
  `id` int(7) NOT NULL,
  `appointment_id` int(7) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `action` varchar(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `appointment_logs`:
--

--
-- Dumping data for table `appointment_logs`
--

INSERT INTO `appointment_logs` (`id`, `appointment_id`, `customer`, `action`, `date`) VALUES
(1, 51, 'Mark G. Lumen', 'Updated', '2018-12-15 05:21:39'),
(2, 48, 'Leah A. Juan', 'Deleted', '2018-12-15 05:53:33'),
(4, 51, 'Mark G. Lumen', 'Updated', '2018-12-15 08:31:14'),
(5, 52, 'Mark Lumen', 'Inserted', '2018-12-16 04:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `notes` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `customer`:
--

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `address`, `phone`, `mail`, `notes`) VALUES
(10, 'Donald Juan', 'Parang Cantilan Surigao del Sur', '09488159946', 'donaldjuan10@gmail.com', 'None'),
(11, 'Joven A. Juan', 'qwerty', 'qwerty', 'qwerty', 'qwerty'),
(12, 'Leah A. Juan', 'Parang, Cantilan, SDS', '09073942485', 'leag@gmail.com', 'NONE'),
(15, 'Ivy D. Paloma', 'Manila Philippines', '09488159946', 'ivy@gmail.com', 'None\r\n'),
(16, 'Mark Lumen', 'Talomo, Davo City', '09105630947', 'lumen@gmail.com', 'None'),
(17, 'Jessa Gruyal', 'Buntalid SDS', '09123456789', 'jesa@gmail.com', 'None');

--
-- Triggers `customer`
--
DROP TRIGGER IF EXISTS `customer_AD`;
DELIMITER $$
CREATE TRIGGER `customer_AD` AFTER DELETE ON `customer` FOR EACH ROW insert into customer_logs values (null, old.id,old.name,'Deleted', NOW())
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `customer_AI`;
DELIMITER $$
CREATE TRIGGER `customer_AI` AFTER INSERT ON `customer` FOR EACH ROW insert into customer_logs values (null, new.id,new.name,'Inserted', NOW())
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `customer_AU`;
DELIMITER $$
CREATE TRIGGER `customer_AU` AFTER UPDATE ON `customer` FOR EACH ROW insert into customer_logs values (null, new.id,old.name,'Updated', NOW())
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_logs`
--

DROP TABLE IF EXISTS `customer_logs`;
CREATE TABLE `customer_logs` (
  `id` int(7) NOT NULL,
  `customer_id` int(7) NOT NULL,
  `name` varchar(50) NOT NULL,
  `action` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `customer_logs`:
--

--
-- Dumping data for table `customer_logs`
--

INSERT INTO `customer_logs` (`id`, `customer_id`, `name`, `action`, `date`) VALUES
(1, 10, 'Donald A. Juan', 'Updated', '2018-12-15 08:31:42'),
(2, 17, 'Jessa Gruyal', 'Inserted', '2018-12-16 04:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory` (
  `id` int(7) NOT NULL,
  `sno` int(10) NOT NULL,
  `invoice_id` varchar(50) NOT NULL,
  `batch` varchar(20) NOT NULL,
  `cost_price` int(6) NOT NULL,
  `sell_price` int(6) NOT NULL,
  `quantity` int(6) NOT NULL,
  `qty_sold` int(6) NOT NULL,
  `expiry_date` varchar(20) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `inventory`:
--

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `sno`, `invoice_id`, `batch`, `cost_price`, `sell_price`, `quantity`, `qty_sold`, `expiry_date`, `datetime`) VALUES
(1, 3, '9', '1', 800, 900, 10, 0, '2018-12-16', '2018-12-08 06:40:21'),
(2, 21, '10', '1', 10, 15, 93, 0, '2018-12-08', '2018-12-08 10:40:58'),
(3, 6, '11', '1', 800, 1000, 0, 0, '2018-12-08', '2018-12-08 11:09:39'),
(4, 4, '12', '1', 400, 500, 0, 0, '2018-12-08', '2018-12-08 11:32:29'),
(5, 4, '13', '2', 450, 500, 10, 0, '2018-12-08', '2018-12-08 14:00:38'),
(6, 6, '14', '2', 800, 1000, 5, 0, '2018-12-15', '2018-12-08 14:01:29'),
(7, 3, '15', '2', 800, 1000, 0, 0, '2018-12-16', '2018-12-15 01:55:11'),
(8, 1, '16', '1', 100, 150, 90, 0, '2019-02-15', '2018-12-15 04:24:17'),
(9, 2, '17', '1', 4000, 4500, 50, 0, '2018-12-15', '2018-12-15 04:27:12'),
(10, 8, '18', '1', 400, 500, 100, 0, '2018-12-16', '2018-12-16 04:27:59'),
(11, 10, '19', '1', 50, 60, 100, 0, '2019-01-03', '2019-01-03 04:39:06'),
(12, 17, '20', '1', 80, 100, 100, 0, '2019-01-03', '2019-01-03 04:49:40');

--
-- Triggers `inventory`
--
DROP TRIGGER IF EXISTS `inventory_AD`;
DELIMITER $$
CREATE TRIGGER `inventory_AD` AFTER DELETE ON `inventory` FOR EACH ROW insert into inventory_logs values (null, old.id,old.cost_price,old.sell_price,old.expiry_date,'Deleted', NOW())
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `inventory_AI`;
DELIMITER $$
CREATE TRIGGER `inventory_AI` AFTER INSERT ON `inventory` FOR EACH ROW insert into inventory_logs values (null, new.id,new.cost_price,new.sell_price,new.expiry_date,'Inserted', NOW())
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `inventory_AU`;
DELIMITER $$
CREATE TRIGGER `inventory_AU` AFTER UPDATE ON `inventory` FOR EACH ROW insert into inventory_logs values (null, new.id,old.cost_price,old.sell_price,old.expiry_date,'Updated', NOW())
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_logs`
--

DROP TABLE IF EXISTS `inventory_logs`;
CREATE TABLE `inventory_logs` (
  `id` int(7) NOT NULL,
  `inventory_id` int(7) NOT NULL,
  `cost_price` int(20) NOT NULL,
  `sell_price` int(20) NOT NULL,
  `expiry_date` varchar(30) NOT NULL,
  `action` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `inventory_logs`:
--

--
-- Dumping data for table `inventory_logs`
--

INSERT INTO `inventory_logs` (`id`, `inventory_id`, `cost_price`, `sell_price`, `expiry_date`, `action`, `date`) VALUES
(1, 1, 800, 1000, '2018-12-30', 'Updated', '2018-12-15 08:46:58'),
(2, 1, 800, 1000, '2018-12-30', 'Updated', '2018-12-15 08:47:46'),
(3, 10, 400, 500, '2018-12-16', 'Inserted', '2018-12-16 04:27:59'),
(4, 8, 100, 150, '2019-02-15', 'Updated', '2018-12-24 19:47:05'),
(5, 6, 800, 1000, '2018-12-15', 'Updated', '2018-12-24 19:47:15'),
(6, 11, 50, 60, '2019-01-03', 'Inserted', '2019-01-03 04:39:06'),
(7, 12, 80, 100, '2019-01-03', 'Inserted', '2019-01-03 04:49:40'),
(8, 10, 400, 500, '2018-12-16', 'Updated', '2019-01-03 04:50:22'),
(9, 10, 400, 500, '2018-12-16', 'Updated', '2019-01-03 04:51:01'),
(10, 10, 400, 500, '2018-12-16', 'Updated', '2019-01-03 04:51:48'),
(11, 10, 400, 500, '2018-12-16', 'Updated', '2019-01-03 04:52:00'),
(12, 10, 400, 500, '2018-12-16', 'Updated', '2019-01-03 04:52:14'),
(13, 10, 400, 500, '2018-12-16', 'Updated', '2019-01-03 04:53:02');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_list`
--

DROP TABLE IF EXISTS `medicine_list`;
CREATE TABLE `medicine_list` (
  `sno` int(7) NOT NULL,
  `trade_name` varchar(50) NOT NULL,
  `generic_name` varchar(50) NOT NULL,
  `type` varchar(20) NOT NULL,
  `sell_price` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `medicine_list`:
--

--
-- Dumping data for table `medicine_list`
--

INSERT INTO `medicine_list` (`sno`, `trade_name`, `generic_name`, `type`, `sell_price`) VALUES
(1, 'Lefcar', 'L-Carnitine', 'Injectables', 150),
(2, 'Tad', 'Glutathione ', 'Tablets', 4500),
(3, 'Snow White', 'Pearl Cream', 'Creams and Ointments', 1000),
(4, 'Glutax', 'Glutathione', 'Injectables', 500),
(5, 'Glutax 10 CRP', 'Glutathione', 'Injectables', 600),
(6, 'Glutax 70000', 'Glutathione', 'Injectables', 1000),
(7, 'Cindella 1200mg', 'Glutathione', 'Injectables', 1000),
(8, 'Miracle White', 'Thiotic Acid', 'Injectables', 500),
(9, 'Cannula', 'None', 'Medical Suppllies', 50),
(10, 'Macroset', 'None', 'Medical Suppllies', 60),
(11, 'Syringes', 'None', 'Medical Suppllies', 60),
(12, 'PNSS 50', 'Sodium Chloride Solution', 'Medical Suppllies', 600),
(13, 'PNSS 100', 'Sodium Chloride Solution', 'Drops', 200),
(15, 'PNSS 500', 'Sodium Chloride Solution', 'Drops', 500),
(16, 'Biocell', ' Superoxide Dismutase', 'Injectables', 500),
(17, 'Collagen', 'Ascorbic Acid-Collagen', 'Capsules', 100),
(18, 'Placenta', 'Ergonovic Malleate', 'Tablets', 800),
(19, 'Vitamin C', 'None', 'Syrup/Suspension', 90);

--
-- Triggers `medicine_list`
--
DROP TRIGGER IF EXISTS `medicinelist_AD`;
DELIMITER $$
CREATE TRIGGER `medicinelist_AD` AFTER DELETE ON `medicine_list` FOR EACH ROW insert into medicine_list_logs values (null, old.sno,old.trade_name,old.generic_name,old.type,old.sell_price, NOW(),'Deleted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `medicinelist_AI`;
DELIMITER $$
CREATE TRIGGER `medicinelist_AI` AFTER INSERT ON `medicine_list` FOR EACH ROW insert into medicine_list_logs values (null, new.sno,new.trade_name,new.generic_name,new.type,new.sell_price, NOW(),'Inserted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `medicinelist_AU`;
DELIMITER $$
CREATE TRIGGER `medicinelist_AU` AFTER UPDATE ON `medicine_list` FOR EACH ROW insert into medicine_list_logs values (null, new.sno,old.trade_name,old.generic_name,old.type,old.sell_price, NOW(),'Updated')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `medicine_list_logs`
--

DROP TABLE IF EXISTS `medicine_list_logs`;
CREATE TABLE `medicine_list_logs` (
  `id` int(7) NOT NULL,
  `sno` int(7) NOT NULL,
  `trade_name` varchar(50) NOT NULL,
  `generic_name` varchar(50) NOT NULL,
  `type` varchar(20) NOT NULL,
  `sell_price` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `medicine_list_logs`:
--

--
-- Dumping data for table `medicine_list_logs`
--

INSERT INTO `medicine_list_logs` (`id`, `sno`, `trade_name`, `generic_name`, `type`, `sell_price`, `date`, `action`) VALUES
(1, 23, 'Dolfenal', 'Paracetamol', 'Tablets', '100', '2018-12-16 04:41:42', 'Inserted'),
(2, 22, 'Dulfenal', 'Biogesic', 'Tablets', '9', '2018-12-16 04:42:21', 'Updated'),
(3, 23, 'Dolfenal', 'Paracetamol', 'Tablets', '100', '2018-12-16 04:42:38', 'Deleted'),
(4, 22, 'Dulfenal', 'Biogesics', 'Tablets', '50', '2018-12-16 04:42:43', 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
CREATE TABLE `purchases` (
  `id` int(7) NOT NULL,
  `supplier` varchar(30) NOT NULL,
  `invoiceNo` varchar(15) NOT NULL,
  `invoiceDate` varchar(15) NOT NULL,
  `notes` varchar(50) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uuid` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `purchases`:
--

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `supplier`, `invoiceNo`, `invoiceDate`, `notes`, `updated`, `uuid`) VALUES
(18, 'John Jay Rivera', 'IDrip2064302', '12/16/2018', 'None', '2018-12-16 04:27:59', '5c15d44f3cdad'),
(19, 'John Jay Rivera', 'IDrip02232', '01/03/2019', 'None', '2019-01-03 04:39:06', '5c2d91ea21986'),
(20, 'Select Supplier', 'IDrip3337283', '01/03/2019', 'None', '2019-01-03 04:49:40', '5c2d94647e3ad');

--
-- Triggers `purchases`
--
DROP TRIGGER IF EXISTS `purchases_AD`;
DELIMITER $$
CREATE TRIGGER `purchases_AD` AFTER DELETE ON `purchases` FOR EACH ROW insert into purchases_logs values (null, old.id,old.supplier,old.uuid,old.invoiceNo,old.invoiceDate,old.notes, NOW(),'Deleted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `purchases_AI`;
DELIMITER $$
CREATE TRIGGER `purchases_AI` AFTER INSERT ON `purchases` FOR EACH ROW insert into purchases_logs values (null, new.id,new.supplier,new.uuid,new.invoiceNo,new.invoiceDate,new.notes, NOW(),'Inserted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `purchases_AU`;
DELIMITER $$
CREATE TRIGGER `purchases_AU` AFTER UPDATE ON `purchases` FOR EACH ROW insert into purchases_logs values (null, new.id,old.supplier,old.uuid,old.invoiceNo,old.invoiceDate,old.notes, NOW(),'Updated')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `purchases_logs`
--

DROP TABLE IF EXISTS `purchases_logs`;
CREATE TABLE `purchases_logs` (
  `id` int(7) NOT NULL,
  `supplier_id` int(7) NOT NULL,
  `supplier` varchar(50) NOT NULL,
  `uuid` int(20) NOT NULL,
  `invoiceNo` varchar(20) NOT NULL,
  `invoiceDate` varchar(20) NOT NULL,
  `notes` longtext NOT NULL,
  `date` varchar(20) NOT NULL,
  `action` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `purchases_logs`:
--

--
-- Dumping data for table `purchases_logs`
--

INSERT INTO `purchases_logs` (`id`, `supplier_id`, `supplier`, `uuid`, `invoiceNo`, `invoiceDate`, `notes`, `date`, `action`) VALUES
(1, 11, 'Donald', 5, 'Rx30323532', '12/08/2018', 'None', '2018-12-16 12:14:45', 'Updated'),
(2, 2, 'Donald A. Juan', 5, 'IDrip22232', '12/08/2018', '', '2018-12-16 12:15:03', 'Updated'),
(3, 1, 'Donald A. Juan', 5, 'IDrip30323532', '12/08/2018', 'None', '2018-12-16 12:15:18', 'Updated'),
(4, 3, 'Donald A. Juan', 5, 'Rx79322002', '12/08/2018', 'None', '2018-12-16 12:15:35', 'Updated'),
(5, 4, 'Donald A. Juan', 5, 'IDrip222523', '12/08/2018', '', '2018-12-16 12:16:01', 'Updated'),
(6, 1, 'Donald A. Juan', 5, 'IDrip30323532', '12/08/2018', 'None', '2019-01-03 12:37:52', 'Deleted'),
(7, 2, 'Donald A. Juan', 5, 'IDrip22232', '12/08/2018', 'None', '2019-01-03 12:37:52', 'Deleted'),
(8, 3, 'Donald A. Juan', 5, 'IDrip79322002', '12/08/2018', 'None', '2019-01-03 12:37:52', 'Deleted'),
(9, 4, 'Donald A. Juan', 5, 'IDrip222523', '12/08/2018', 'None', '2019-01-03 12:37:53', 'Deleted'),
(10, 19, 'John Jay Rivera', 5, 'IDrip02232', '01/03/2019', 'None', '2019-01-03 12:39:06', 'Inserted'),
(11, 20, 'Select Supplier', 5, 'IDrip3337283', '01/03/2019', 'None', '2019-01-03 12:49:40', 'Inserted');

-- --------------------------------------------------------

--
-- Table structure for table `sales_list`
--

DROP TABLE IF EXISTS `sales_list`;
CREATE TABLE `sales_list` (
  `transaction_id` int(11) NOT NULL,
  `invoice` varchar(100) NOT NULL,
  `product` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `sno` varchar(10) NOT NULL,
  `price` varchar(100) NOT NULL,
  `profit` int(7) NOT NULL,
  `discount` varchar(100) NOT NULL,
  `date` varchar(500) NOT NULL,
  `time` varchar(10) NOT NULL,
  `customer` varchar(100) NOT NULL,
  `entrant` varchar(20) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `sales_list`:
--

--
-- Dumping data for table `sales_list`
--

INSERT INTO `sales_list` (`transaction_id`, `invoice`, `product`, `quantity`, `amount`, `sno`, `price`, `profit`, `discount`, `date`, `time`, `customer`, `entrant`, `datetime`) VALUES
(70, 'IDrip325023', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '11:17 am', 'Donald A. Juan', 'Administrator', '2018-12-08 08:18:28'),
(71, 'IDrip325023', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '11:18 am', 'Donald A. Juan', 'Administrator', '2018-12-08 08:18:35'),
(72, 'IDrip3032903', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '11:27 am', 'Joven A. Juan', 'Administrator', '2018-12-08 08:28:02'),
(73, 'IDrip322322', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '4:31 pm', 'Joven A. Juan', 'Administrator', '2018-12-08 08:31:16'),
(74, 'IDrip32609322', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '4:39 pm', 'Joven A. Juan', 'Administrator', '2018-12-08 08:39:25'),
(75, 'IDrip32609322', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '4:39 pm', '', 'Administrator', '2018-12-08 08:39:28'),
(76, 'IDrip32609322', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '4:39 pm', '', 'Administrator', '2018-12-08 08:39:32'),
(77, 'IDrip5323222', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '4:39 pm', 'Joven A. Juan', 'Administrator', '2018-12-08 08:41:13'),
(78, 'IDrip8223003', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '4:51 pm', 'Joven A. Juan', 'Administrator', '2018-12-08 08:51:44'),
(79, 'IDrip8223003', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '4:51 pm', '', 'Administrator', '2018-12-08 08:51:48'),
(80, 'IDrip8223003', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '4:51 pm', '', 'Administrator', '2018-12-08 08:51:51'),
(81, 'IDrip002223', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '5:06 pm', 'Joven A. Juan', 'Administrator', '2018-12-08 09:06:57'),
(82, 'IDrip002223', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '5:06 pm', '', 'Administrator', '2018-12-08 09:07:00'),
(83, 'IDrip002223', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '5:07 pm', '', 'Administrator', '2018-12-08 09:07:03'),
(84, 'IDrip2060', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '', 'Leah A. Juan', 'Administrator', '2018-12-08 09:10:06'),
(85, 'Rx2060', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '5:10 pm', '', 'Administrator', '2018-12-08 09:10:09'),
(86, 'Rx2060', '1', '1', '1000', '3', '1000', 200, '', '12/08/18', '5:10 pm', '', 'Administrator', '2018-12-08 09:10:11'),
(93, 'IDrip07303', '2', '1', '15', '21', '15', 5, '', '12/15/18', '9:45 am', 'Joven A. Juan', 'donjuan_arienza', '2018-12-15 01:45:48'),
(94, 'IDrip332720', '4', '1', '500', '4', '500', 100, '', '12/15/18', '9:45 am', 'Ivy D. Paloma', 'donjuan_arienza', '2018-12-15 01:46:07'),
(95, 'IDrip02203', '8', '10', '1500', '1', '150', 500, '', '12/25/18', '3:46 am', 'Donald Juan', 'donjuan_arienza', '2018-12-24 19:47:05'),
(96, 'IDrip02203', '6', '5', '5000', '6', '1000', 1000, '', '12/25/18', '3:47 am', '', 'donjuan_arienza', '2018-12-24 19:47:15');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `country` varchar(50) NOT NULL,
  `currency` varchar(6) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `level` int(3) NOT NULL,
  `timezone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `settings`:
--

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `address`, `address2`, `email`, `phone`, `country`, `currency`, `logo`, `level`, `timezone`) VALUES
(1, 'IDrip Gluta Center', 'Blush + Blow Beauty Salon', 'Boulevard, Davao City', 'idrip@gmail.com', '09105630947', 'Philippines', 'Php', '743999.png', 10, 'Asia/Singapore');

--
-- Triggers `settings`
--
DROP TRIGGER IF EXISTS `settings_AD`;
DELIMITER $$
CREATE TRIGGER `settings_AD` AFTER DELETE ON `settings` FOR EACH ROW insert into settings_logs values (null, old.id,old.name,old.address,old.address2,old.email,old.phone,old.country,old.currency,old.logo,old.level,old.timezone, NOW(),'Deleted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `settings_AI`;
DELIMITER $$
CREATE TRIGGER `settings_AI` AFTER INSERT ON `settings` FOR EACH ROW insert into settings_logs values (null, new.id,new.name,new.address,new.address2,new.email,new.phone,new.country,new.currency,new.logo,new.level,new.timezone, NOW(),'Inserted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `settings_AU`;
DELIMITER $$
CREATE TRIGGER `settings_AU` AFTER UPDATE ON `settings` FOR EACH ROW insert into settings_logs values (null, new.id,old.name,old.address,old.address2,old.email,old.phone,old.country,old.currency,old.logo,old.level,old.timezone, NOW(),'Updated')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `settings_logs`
--

DROP TABLE IF EXISTS `settings_logs`;
CREATE TABLE `settings_logs` (
  `id` int(7) NOT NULL,
  `settings_id` int(7) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(50) NOT NULL,
  `address2` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `country` varchar(50) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `level` varchar(10) NOT NULL,
  `timezone` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `settings_logs`:
--

--
-- Dumping data for table `settings_logs`
--

INSERT INTO `settings_logs` (`id`, `settings_id`, `name`, `address`, `address2`, `email`, `phone`, `country`, `currency`, `logo`, `level`, `timezone`, `date`, `action`) VALUES
(1, 1, 'IDrip Gluta Center', 'Boulevard, Davao City, 8000', 'Boulevard, Davao City 8000', 'idrip@gmail.com', '09105630947', 'Philippines', 'Php', '743999.png', '10', 'Asia/Singapore', '2018-12-15 20:29:51', 'Updated');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `contact_person` varchar(20) NOT NULL,
  `notes` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `supplier`:
--

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `address`, `phone`, `contact_person`, `notes`) VALUES
(7, 'Donald  Juan', 'Burgos Street, Davao City', '09488159946', 'Dondon', 'Trial'),
(9, 'John Jay Rivera', 'Matina, Davao City', '09105630947', 'Boy', 'None');

--
-- Triggers `supplier`
--
DROP TRIGGER IF EXISTS `supplier_AD`;
DELIMITER $$
CREATE TRIGGER `supplier_AD` AFTER DELETE ON `supplier` FOR EACH ROW insert into supplier_logs values (null, old.id,old.name,old.address,old.phone,old.contact_person,old.notes, NOW(),'Deleted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `supplier_AI`;
DELIMITER $$
CREATE TRIGGER `supplier_AI` AFTER INSERT ON `supplier` FOR EACH ROW insert into supplier_logs values (null, new.id,new.name,new.address,new.phone,new.contact_person,new.notes, NOW(),'Inserted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `supplier_AU`;
DELIMITER $$
CREATE TRIGGER `supplier_AU` AFTER UPDATE ON `supplier` FOR EACH ROW insert into supplier_logs values (null, new.id,old.name,old.address,old.phone,old.contact_person,old.notes, NOW(),'Updated')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_logs`
--

DROP TABLE IF EXISTS `supplier_logs`;
CREATE TABLE `supplier_logs` (
  `id` int(7) NOT NULL,
  `supplier_id` int(7) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `contact_person` varchar(30) NOT NULL,
  `notes` longtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `supplier_logs`:
--

--
-- Dumping data for table `supplier_logs`
--

INSERT INTO `supplier_logs` (`id`, `supplier_id`, `name`, `address`, `phone`, `contact_person`, `notes`, `date`, `action`) VALUES
(1, 7, 'Donald  Juan', 'Burgos Street, Davao City', '09488159946', 'Dondon', 'None', '2018-12-15 19:51:09', 'Updated');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `userid` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `mail` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `users`:
--

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `userid`, `password`, `mail`) VALUES
(32, 'Irish F. Donor', 'irish@idrip2018', '3201f0c28e490b3dc14d444cf2243b66', 'dispenser@dispenser.com'),
(35, 'Dummy Account', 'idrip123', 'fff42bd6dcf046f68c853a8a1d95f7c2', 'idrip@gmail.com'),
(36, 'Donald Juan', 'donjuan_arienza', '3201f0c28e490b3dc14d444cf2243b66', 'donaldjuan10@gmail.com');

--
-- Triggers `users`
--
DROP TRIGGER IF EXISTS `users_AD`;
DELIMITER $$
CREATE TRIGGER `users_AD` AFTER DELETE ON `users` FOR EACH ROW insert into users_logs values (null, old.id,old.name,old.userid,old.password,old.mail, NOW(),'Deleted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `users_AI`;
DELIMITER $$
CREATE TRIGGER `users_AI` AFTER INSERT ON `users` FOR EACH ROW insert into users_logs values (null,new.id,new.name,new.userid,new.password,new.mail, NOW(), 'Inserted')
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `users_AU`;
DELIMITER $$
CREATE TRIGGER `users_AU` AFTER UPDATE ON `users` FOR EACH ROW insert into users_logs values (null,new.id,old.name,old.userid,old.password,old.mail, NOW(), 'Updated')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users_logs`
--

DROP TABLE IF EXISTS `users_logs`;
CREATE TABLE `users_logs` (
  `id` int(7) NOT NULL,
  `users_id` int(7) NOT NULL,
  `name` varchar(30) NOT NULL,
  `userid` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `users_logs`:
--

--
-- Dumping data for table `users_logs`
--

INSERT INTO `users_logs` (`id`, `users_id`, `name`, `userid`, `password`, `mail`, `date`, `action`) VALUES
(4, 34, 'Mark G. Lumen', 'lumen12345', 'de965683457107db0f649378aae305b8', 'lumen@gmail.com', '2018-12-15 19:16:25', 'Updated'),
(5, 34, 'Mark  Lumen', 'lumen12345', 'de965683457107db0f649378aae305b8', 'lumen@gmail.com', '2018-12-15 19:16:49', 'Deleted'),
(6, 35, 'Dummy Account', 'idrip123', 'fff42bd6dcf046f68c853a8a1d95f7c2', 'idrip@gmail.com', '2018-12-15 19:17:57', 'Updated'),
(7, 31, 'donjuan_arienza', 'donjuan_arienza', '3201f0c28e490b3dc14d444cf2243b66', 'admin@admin.com', '2018-12-15 19:18:12', 'Deleted'),
(8, 35, 'Dummy Account', 'idrip12345', 'fff42bd6dcf046f68c853a8a1d95f7c2', 'idrip@gmail.com', '2018-12-15 19:19:18', 'Updated'),
(9, 36, 'Donald Juan', 'donjuan_arienza', '3201f0c28e490b3dc14d444cf2243b66', 'donaldjuan10@gmail.com', '2018-12-15 19:20:11', 'Inserted');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment_logs`
--
ALTER TABLE `appointment_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_logs`
--
ALTER TABLE `customer_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_logs`
--
ALTER TABLE `inventory_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicine_list`
--
ALTER TABLE `medicine_list`
  ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `medicine_list_logs`
--
ALTER TABLE `medicine_list_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases_logs`
--
ALTER TABLE `purchases_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_list`
--
ALTER TABLE `sales_list`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_logs`
--
ALTER TABLE `settings_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_logs`
--
ALTER TABLE `supplier_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_logs`
--
ALTER TABLE `users_logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `appointment_logs`
--
ALTER TABLE `appointment_logs`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `customer_logs`
--
ALTER TABLE `customer_logs`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `inventory_logs`
--
ALTER TABLE `inventory_logs`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `medicine_list`
--
ALTER TABLE `medicine_list`
  MODIFY `sno` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `medicine_list_logs`
--
ALTER TABLE `medicine_list_logs`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `purchases_logs`
--
ALTER TABLE `purchases_logs`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sales_list`
--
ALTER TABLE `sales_list`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings_logs`
--
ALTER TABLE `settings_logs`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `supplier_logs`
--
ALTER TABLE `supplier_logs`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users_logs`
--
ALTER TABLE `users_logs`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
