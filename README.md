The purpose of the project entitled as “IDrip Management System” is to computerize the Front Office Management of IDrip Gluta Center which is located inside Blush + Blow Salon whom being managed by Ms. Irish Lyn F. Donor (a registered nurse). This project also aims to develop software which is user friendly, simple, fast, and effective. It deals with the collection of customer’s information, etc, of the clinic. Traditionally, it was done manually. The main function of the system was to register and store customer details and retrieve these details when required, and also to manipulate these details meaningfully. System input contains customer details, while system output is to get these details on to the screen. The IDrip Management System can be accessed using a predefined username and password. It is accessible only to the manager. The data can be retrieved easily.  The data are well protected for personal use and makes the data processing efficient.



Requirements:   [ Xammp Web Server ] [ Test Mail Server tool ]



Sample Account: [ Username: idrip123 ]  [ Password: idrip123 ]



Database Name:  [ idrip1.sql ]
